package some.example;

import java.io.IOException;
import java.net.BindException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.json.JSONObject;

public class JSONServer {

    private int _connectedSocketLimit = 10000;
    private byte[] _allInterfaces = new byte[] {0,0,0,0};
    private ErrorCallback _onError;

    private ServerSocket _server;
    private boolean _connected;

    public final static int clientTimeout = 5000; // 5 seconds
    public final List<JSONClient> clients = Collections.synchronizedList(new ArrayList<JSONClient>()); // solving concurenncy issues

    public void setOnError(ErrorCallback handler) {
        _onError = handler;
    }

    public boolean isConnected() {
        return _connected;
    }

    public void start(int port) throws IOException {
        _server = new ServerSocket();
        _server.setReuseAddress(true);
        try {
        	_server.bind(new InetSocketAddress(InetAddress.getByAddress(_allInterfaces), port));
        } catch (BindException err) {
        	err.printStackTrace();
        	return;
        }
        Thread incomeConnection = new Thread(new Runnable() {
            public void run() {
                while (_connected) {
                    Socket client = null;
                    try {
                        client = _server.accept();
                    } catch (SocketTimeoutException err) {
                        continue;
                    } catch (IOException err) {
                        if (_onError != null) {
                            _onError.onError(err);
                        }
                    }
                    if (client != null) {
                        handleClient(new JSONClient(client));
                    }
                }
            }
        }, "JSONServer_incomeConnection");

        _connected = true;
        incomeConnection.start();
    }

    public void close() {
        if (_server != null) {
            try {
                _server.close();
                _connected = false;
                _server = null;
            } catch (IOException err) {
                err.printStackTrace();
            }
        }
    }

    @Override
    protected void finalize() throws Throwable {
        close();
        super.finalize();
    }

    private void handleClient(final JSONClient client) {
//        System.out.println("Clients length = "+clients.size());
    	if (clients.size() > _connectedSocketLimit) {
            System.out.println("Exceeded number of allowd connections");
            return;
        }
        Thread clientThread = new Thread(new Runnable() {
            public void run() {
                try {
                    clients.add(client);
                    client.setSoTimeout(clientTimeout);
                    while (_connected) {
                        JSONPacket request = client.readPacket();
                        if (request == null) {
                            break;
                        }
                        JSONObject rPayload = request.getPayload();
                        JSONPacket response = new JSONPacket(rPayload);
//                        stdoutPrinter("Writing to response to client: "+response.getPayload());
                        client.writePacket(response);
                    }
                } catch (SocketTimeoutException err) {
                    if (_onError != null) {
                        _onError.onError(err);
                    }
                } catch (IOException err) {
                    err.printStackTrace();
                } finally {
                    client.close();
                    clients.remove(client);
                }
            }
        }, "JSONServer_handleClient");
        clientThread.start();
    }

    public interface RequestCallback {
        public JSONPacket onRequest(JSONPacket request);
    }

    public interface ErrorCallback {
        public void onError(Exception e);
    }

    public void stdoutPrinter(String msg) {
        System.out.println("SERVER:: "+msg);
    }

}
