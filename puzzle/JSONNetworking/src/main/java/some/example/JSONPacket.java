package some.example;

import org.json.JSONObject;

public class JSONPacket {
    private JSONObject payload;

    public JSONPacket(JSONObject pPayload) {
		payload = pPayload;
	}

	public JSONObject getPayload(){
		return payload;
	}

//    public String toString() {
//        return payload.toString();
//    }
}
