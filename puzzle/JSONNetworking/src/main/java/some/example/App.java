package some.example;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.json.JSONObject;

// import org.json.JSONObject;
/**
 * mvn compile
 * mvn exec:java -Dexec.mainClass="some.example.App"
 *
 */
public class App
{
	private static final int PORT = 8888;
	private static final String INTERFACE = "0.0.0.0"; // all IPv4 address space
	private static final boolean VERBOSE = false; // Change this to get more output
	private static final int SERIAL_TEST_NUM = 100; // Number serial clients to test
	private static final int PARALLEL_TEST_NUM = 100; // Number of parallel threads to span into test
	
	public static void main( String[] args ) {
        
    	System.out.println( "\n#############\nApp Starting\n#############\n" );
        
        JSONServer _server = new JSONServer();        
        try {
        	_server.start(PORT);
        } catch (IOException err) {
    		err.printStackTrace();
        }
        
        // ################
        // ###  TESTS   ###
        // ################
        
        // Single client
        if (test_single(1)) {
        	System.out.println("Passed single client test");
        } else {
        	System.out.println("FAILED single client test");
        }
        
        // Serial multiple clients
        if (test_serial(SERIAL_TEST_NUM)) {
        	System.out.println("Passed " + SERIAL_TEST_NUM + " serial clients test");
        } else {
        	System.out.println("Failed " + SERIAL_TEST_NUM + " serial clients test");
        }

        // Parallel multiple clients
//        if (test_parallel(PARALLEL_TEST_NUM)) {
        if (test_parallel2()) {
        	System.out.println("Passed " + PARALLEL_TEST_NUM + " parallel clients test");
        } else {
        	System.out.println("Failed " + PARALLEL_TEST_NUM + " parallel clients test");
        }
        
        _server.close();

        System.out.println( "\n#############\nApp Finished\n#############\n\n" );
        System.exit(0);
    }
    
    @SuppressWarnings("static-access")
	public static boolean test_single(int n) {
      JSONClient _client = new JSONClient();
      JSONObject rawPacket = new JSONObject();
      rawPacket.put("client_num", n);
      JSONPacket _packet = new JSONPacket(rawPacket);
      try {
    	  verbose("Sending packet: " + _packet.getPayload() + " to server");
    	  JSONPacket result = _client.getResponse(INTERFACE, PORT, _packet);
    	  if (result != null) {
    		  verbose("Recieved response: " + result.getPayload() + " from server");    		  
    		  return _packet.getPayload().toString().equals(result.getPayload().toString());
    	  }
      } catch (IOException err) {
			err.printStackTrace();
			System.out.println(err.toString());
			System.out.println("caught an exception");
      }
      return false;
    }
    
    public static boolean test_serial(int numOfClients) {
    	boolean result = true;
    	for (int i = 0; i < numOfClients; i++) {
    		result = result && test_single(i);
    	}
    	return result;
    }

    public static boolean test_parallel(int numThreads) {
    	boolean res = true;
    	ClientThread[] threadsArr = new ClientThread[numThreads];
    	for (int i = 0; i < numThreads; i++) {
    		threadsArr[i] = new ClientThread("client_"+Integer.toString(i));
    		threadsArr[i].start();    		
    	}
    	for (int i = 0; i < numThreads; i++) {
    		res = res && threadsArr[i].getResult();
    	}
    	return res;
    }
    
    public static boolean test_parallel2() {
        boolean result = true;
    	ExecutorService pool = Executors.newFixedThreadPool(PARALLEL_TEST_NUM);        
        Set<Future<Boolean>> set = new HashSet<Future<Boolean>>();
        try {
        	for (int i = 0; i < PARALLEL_TEST_NUM; i++) {
        		Callable<Boolean> clThread = new ClientCallable();
        		Future<Boolean> future = pool.submit(clThread);
        		set.add(future);
        	}
        	for (Future<Boolean> future : set) {
        		result = result && future.get();
        	}
        	return result;
        } catch(final InterruptedException ex) {
            ex.printStackTrace();
        } catch(final ExecutionException ex) {
            ex.printStackTrace();
        }
        pool.shutdownNow();
        return false;
    }
    
    public static void verbose(String msg) {
    	if (VERBOSE) {
    		System.out.println(msg);
    	}
    }
}
