package some.example;

import java.io.IOException;

import org.json.JSONObject;

public class ClientThread implements Runnable {
	
	private static final int PORT = 8888;
	private static final String INTERFACE = "0.0.0.0";
	private static final int runNum = 1;
	private volatile boolean threadResult = false;
	
	private String clientName;
	private JSONObject rawPacket;
	private JSONClient client;
	private Thread t;
	
	ClientThread(String cName) {
		clientName = cName;
		rawPacket = new JSONObject();
		client = new JSONClient();
//		System.out.println("Creating " +  clientName );
	}
	
	public String getClientName() {
		return this.clientName;
	}
	
	public void setPacket(String key, String val) {
		rawPacket.put(key, val);
	}
	
	public boolean getResult() {
		return this.threadResult;
	}
	
	@SuppressWarnings("static-access")
	public void run() {
		JSONPacket result;
		for (int i = 0; i < runNum; i++) {
			this.setPacket("packet_num", Integer.toString(i));
			JSONPacket _packet = new JSONPacket(rawPacket);
			try {
				result = this.client.getResponse(INTERFACE, PORT, _packet);
				if (result != null) {
//					System.out.println("Client " + this.getClientName() + " recieved response: " + result.getPayload());
					this.threadResult = _packet.getPayload().toString().equals(result.getPayload().toString());
				}
			} catch (IOException err) {
				err.printStackTrace();
				System.out.println("Exception in "+this.getClientName());
			}        			
		}
	}
	
	public void start() {
		if (t == null) {
			t = new Thread (this, clientName);
			t.start();
		}
	}        	
} // END OF NESTED CLASS

