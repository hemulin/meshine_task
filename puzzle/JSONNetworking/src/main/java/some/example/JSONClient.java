package some.example;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;

import org.json.JSONObject;

public class JSONClient {

    private boolean _connected;
    private Socket _client;


    public static JSONPacket getResponse(String host, int port, JSONPacket request) throws IOException {
        JSONClient _client = new JSONClient();
        try {
            _client.connect(new InetSocketAddress(host, port));
//            stdoutPrinter("Established connection to server");
            _client.writePacket(request);
            JSONPacket res = _client.readPacket(); 
            return res;
        } finally {
            _client.close();
        }
    }

    public JSONClient() {
        _client = new Socket();
        _connected = false;
    }

    protected JSONClient(Socket baseClient) {
        _client = baseClient;
        _connected = baseClient.isConnected() && !baseClient.isClosed();
    }

    public boolean isConnected() {
        return _connected;
    }

    public void connect(SocketAddress endpoint) throws IOException {
        _client.connect(endpoint);
        _connected = (_client.isConnected()) ? true : false;
    }

    public void close() {
        try {
            _client.close();
            _connected = false;
        } catch (IOException err) {
            err.printStackTrace();
        }
    }

    /**
     * Writes <code>packet</code> to the <code>Socket</code>.
     *
     * @param packet
     *            The <code>JSONPacket</code> to write to <code>Socket</code>.
     * @throws IOException
     *             If an IO/Error has occurred.
     */
    public void writePacket(JSONPacket packet) throws IOException {
        try {
            OutputStream outStream = _client.getOutputStream();
            byte[] rawPayload = packet.getPayload().toString().getBytes();
            byte[] buf = ByteBuffer.allocate(Byte.SIZE + rawPayload.length)
            				.put((byte) rawPayload.length)
            				.put(rawPayload)
            				.array();
//            stdoutPrinter("Writing packet: "+ new String(buf));
            outStream.write(buf);
            outStream.flush();
        } catch (IOException err) {
            close();
            throw err;
        }
    }

    /**
     * Reads and creates a <code>JSONPacket</code> from the <code>Socket</code><br>
     * <br>
     * If <code>null</code> is returned, the other side of the
     * <code>Socket</code> should be treated as incompatible, and the
     * <code>Socket</code> be closed immediately.
     *
     * @param str
     *            The stream to read the <code>JSONPacket</code> from.
     * @return A <code>JSONPacket</code> representing the data from the stream,
     *         or <code>null</code> if there were no bytes to read or the data
     *         does not follow the format.
     * @throws IOException
     *             If an IO/Error has occurred.
     */
    public JSONPacket readPacket() throws IOException {
    	JSONPacket res = null;
        try {
            InputStream inStream = _client.getInputStream();
            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            
            int sizeIndicator = inStream.read(); // reading the first byte of the stream as int
            
            byte[] dataBuf = new byte[sizeIndicator];            
            int readBytes = 0;            
           	while (readBytes < sizeIndicator) {
           		readBytes = inStream.read(dataBuf);
           		outStream.write(dataBuf);
           	}
           	// Finished receiving all the payload into the outStream
           	byte data[] = outStream.toByteArray();
           	if (data.length == 0) {
           		return res;
           	}
           	JSONObject payloadData = new JSONObject(new String(data));
           	res = new JSONPacket(payloadData);
//           	stdoutPrinter("Recieved packet: "+res.getPayload());
        } catch (IOException err) {
            close();
            throw err;
        }
        return res;
    }

    public void setSoTimeout(int timeout) throws SocketException {
        _client.setSoTimeout(timeout);
    }

    public static void stdoutPrinter(String msg) {
        System.out.println("CLIENT:: "+msg);
    }

}
