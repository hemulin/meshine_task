package some.example;

import java.io.IOException;
import java.util.concurrent.Callable;

import org.json.JSONObject;

public class ClientCallable implements Callable<Boolean>{
	private static final int PORT = 8888;
	private static final String INTERFACE = "0.0.0.0";
	
	public Boolean call() {		
		JSONClient _client = new JSONClient();
		JSONObject rawPacket = new JSONObject();
		rawPacket.put("thread", "client");
		JSONPacket _packet = new JSONPacket(rawPacket);
		try {
	    	  @SuppressWarnings("static-access")
	    	  JSONPacket result = _client.getResponse(INTERFACE, PORT, _packet);
	    	  if (result != null) {
	    		  return _packet.getPayload().toString().equals(result.getPayload().toString());
	    	  }
		} catch (IOException err) {
				err.printStackTrace();
				System.out.println(err.toString());
				System.out.println("caught an exception");
		}
		return false;
    }
}
